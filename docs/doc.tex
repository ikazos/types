\documentclass{article}

% \usepackage[margin=1in]{geometry}

\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{prftree}

\usepackage{tabularx}
\newcolumntype{Y}{>{\centering\arraybackslash\(}X<{\)}}
% \newcolumntype{Y}{>{\centering\arraybackslash}X}

\newcommand{\types}{\texttt{types}}
\newcommand{\cands}{\texttt{cands}}
\newcommand{\AlgoM}{\(\mathcal{M}\)}
\newcommand{\code}[1]{\texttt{#1}}

\newcommand{\IsNum}[1]{{#1}\;\textsf{num}}
\newcommand{\IsBool}[1]{{#1}\;\textsf{bool}}
\newcommand{\IsTerm}[1]{{#1}\;\textsf{term}}
\newcommand{\IsType}[1]{{#1}\;\textsf{type}}
\newcommand{\IsVar}[1]{{#1}\;\textsf{var}}
\newcommand{\NumType}{\code{Num}}
\newcommand{\BoolType}{\code{Bool}}
\newcommand{\UnitType}{\code{Unit}}

\newcommand{\IsFeature}[1]{{#1}\;\textsf{feature}}
\newcommand{\IsUsize}[1]{{#1}\;\textsf{usize}}

\newcommand{\Unit}{\code{<>}}
\newcommand{\TypedLam}[3]{\code{fn }{#1}\code{: }{#2}\code{ => }{#3}}
\newcommand{\Lam}[2]{\code{fn }{#1}\code{ => }{#2}}
\newcommand{\App}[2]{\code{(}{#1}\code{) }{#2}}
\newcommand{\Let}[3]{\code{let }{#1}\code{ = }{#2}\code{ in }{#3}}
\newcommand{\Fix}[3]{\code{fun }{#1}\code{ }{#2}\code{ = }{#3}}

\title{A Guide to \types}
\author{ikazos}

\begin{document}

\maketitle

\types{} implements Lee \& Yi's (1998) type inference algorithm, also known as Algorithm \AlgoM.  For type unification, we implement the simple algorithm described in Frank Pfenning's lecture notes.

\section{What is Algorithm \AlgoM?}

Algorithm \AlgoM, or just \AlgoM, is a top-down type inference algorithm for the Hindley-Milner let-polymorphic type system.

\subsection{What is a type system?}

Every programming language has a syntax, which is a set of rules that determine what constitutes a well-formed program.  For examples, in a simple programming language, you might have these rules:

\begin{table}[!ht]
    \begin{tabularx}{\linewidth}{YYY}
        \prftree[r]{Num}
            {\IsNum{n}}
            {\IsTerm{n}} &
        \prftree[r]{Bool}
            {\IsBool{b}}
            {\IsTerm{b}} &
        \prftree[r]{Plus}
            {\IsTerm{m_1}}
            {\IsTerm{m_2}}
            {\IsTerm{m_1 \code{+} m_2}}
    \end{tabularx}
\end{table}

In this language, numbers and Boolean values (bools) are terms, and two terms connected by a plus sign is a term.  A term is a well-formed program.

Well-formed programs in this language can run into runtime errors.  For example, \code{2+true}.  Why do we get this error?  Because we can't add numbers and Boolean values together -- they have different types.

A type system extends the syntax of a programming language.  It defines the rules that assign a type to each well-formed term.  In general, a term is either a value or an expression; the difference between them is that a value cannot be further evaluated, while an expression can be evaluated into another expression or a value.  In our previous example, number terms \( n \) and bool terms \( b \) are values, while ``sums'', i.e. \( m_1 \code{+} m_2 \) are expressions.

It's easy to think about what types a type system should assign to values (numbers are numbers, bools are bools, etc.).  But what about expressions?  An expression should be assigned the type of the value it eventually evaluates to.  A type system for our example language might define the following rules:

\begin{table}[!ht]
    \begin{tabularx}{\linewidth}{YYY}
        \prftree[r]{Num}
            {\IsNum{n}}
            {n : \NumType} &
        \prftree[r]{Bool}
            {\IsBool{b}}
            {b : \BoolType} &
        \prftree[r]{Sum}
            {m_1 : \NumType}
            {m_2 : \NumType}
            {m_1 \code{+} m_2 : \NumType}
    \end{tabularx}
\end{table}

By these rules, we cannot give a typing judgment for the term \code{2+true}.

A slightly more complicated programming language might have variables, lambda abstractions and applications.  We extend the syntax like this:

\begin{table}[!htp]
    \begin{tabularx}{\linewidth}{Y}
        \prftree[r]{Variable}
            {\IsVar{x}}
            {\IsTerm{x}} \\[1em]
        \prftree[r]{Abstraction}
            {\IsVar{x}}
            {\IsType{\tau}}
            {\IsTerm{m}}
            {\IsTerm{\TypedLam{x}{\tau}{m}}} \\[1em]
        \prftree[r]{Application}
            {\IsTerm{m_1}}
            {\IsTerm{m_2}}
            {\IsTerm{\App{m_1}{m_2}}}
    \end{tabularx}
\end{table}

Let's say we try to give a type for \code{\TypedLam{x}{\NumType}{x}}.  We know the argument type, which is \( \tau \); we only need to know the return type of this function.  The body of this function is the variable \code{x}; how do we know its type?  Because the signature of this function specified that \code{x} has type \( \tau \) in the function body.  When we make the transition from giving a type to the function to giving a type to the function body, we need to pass on this information about the type \code{x} has in this environment.

To do that, we add type contexts \(\Gamma\) to typing rules.  Type contexts map variables to their types.  The typing rule for a variable performs a lookup in the type context.  The typing rule for a lambda abstraction adds a new mapping to the type context.

\begin{table}[!htp]
    \begin{tabularx}{\linewidth}{Y}
        \prfbyaxiom{Variable}
            {\Gamma, x : \tau \vdash x : \tau} \\[1em]
        \prftree[r]{Abstraction}
            {\Gamma, x : \tau_1 \vdash m : \tau_2}
            {\Gamma \vdash \TypedLam{x}{\tau_1}{m} : \tau_1 \rightarrow \tau_2} \\[1em]
        \prftree[r]{Application}
            {\Gamma \vdash m_1 : \tau_1 \rightarrow \tau_2}
            {\Gamma \vdash m_2 : \tau_1}
            {\Gamma \vdash \App{m_1}{m_2} : \tau_2}
    \end{tabularx}
\end{table}

An even more complicated programming language might have let-bindings, and allow recursive functions.  These might have the following syntax:

\begin{table}[!htp]
    \begin{tabularx}{\linewidth}{Y}
        \prftree[r]{Let}
            {\IsVar{x}}
            {\IsTerm{m_0}}
            {\IsTerm{m_1}}
            {\IsTerm{\Let{x}{m_0}{m_1}}} \\[1em]
        \prftree[r]{Fix}
            {\IsTerm{f}}
            {\IsTerm{x}}
            {\IsTerm{m}}
            {\IsTerm{\Fix{f}{x}{m}}}
    \end{tabularx}
\end{table}

They would have the following typing rules:

\begin{table}[!htp]
    \begin{tabularx}{\linewidth}{Y}
        \prftree[r]{Let}
            {\Gamma \vdash e_0 : \tau_1}
            {\Gamma, x : \tau_1 \vdash e_1 : \tau_2}
            {\Gamma \vdash \Let{x}{e_0}{e_1} : \tau_2} \\[1em]
        \prftree[r]{Fix}
            {\Gamma, f : \tau_1 \rightarrow \tau_2, x : \tau_1 \vdash e : \tau_2}
            {\Gamma \vdash \Fix{f}{x}{e} : \tau_1 \rightarrow \tau_2}
    \end{tabularx}
\end{table}

\subsection{What is the Hindley-Milner let-polymorphic type system?}

A polymorphic type system allows a term to have a polymorphic type, which can then be instantiated into different types and used in different environments.  For example, the identity function for numbers \code{\TypedLam{x}{\NumType}{x}} and that for bools \code{\TypedLam{x}{\BoolType}{x}} look almost the same, but they have different types: the former has type \( \NumType \rightarrow \NumType \), and the latter \( \BoolType \rightarrow \BoolType \).  But what if we have a ``generic'' function that looks like \code{\Lam{x}{x}} without an argument type specification, and said this function has type \( \tau \rightarrow \tau \) for all types \( \tau \)?  It would be convenient to be able to use this function with numbers and bools alike.

The way to express that idea is that we can say \code{\Lam{x}{x}} has a polymorphic type (also known as a polytype, or a type scheme): \( \forall \tau . \tau \rightarrow \tau \).  This type can be instantiated into more ``concrete types'', such as \( \NumType \rightarrow \NumType \) and \( \BoolType \rightarrow \BoolType \).  Formally, we say that these two types are subtypes of the type scheme \( \forall \tau . \tau \rightarrow \tau \).

In \types, we assume a programming that has the following syntax, which should now be familiar:

\begin{table}[!htp]
    \begin{tabularx}{\linewidth}{YY}
        \prfbyaxiom{Unit}
            {\IsTerm{\Unit}} &
        \prftree[r]{Variable}
            {\IsVar{x}}
            {\IsTerm{x}} \\[1em]
        \prftree[r]{Abstraction}
            {\IsVar{x}}
            {\IsTerm{m}}
            {\IsTerm{\Lam{x}{m}}} &
        \prftree[r]{Application}
            {\IsTerm{m_1}}
            {\IsTerm{m_2}}
            {\IsTerm{\App{m_1}{m_2}}} \\[1em]
        \prftree[r]{Let}
            {\IsVar{x}}
            {\IsTerm{m_0}}
            {\IsTerm{m_1}}
            {\IsTerm{\Let{x}{m_0}{m_1}}} &
        \prftree[r]{Fix}
            {\IsVar{f}}
            {\IsVar{x}}
            {\IsTerm{m}}
            {\IsTerm{\Fix{f}{x}{m}}}
    \end{tabularx}
\end{table}

The typing rules also need updated.  The type context is now a map from variables to type schemes instead of types, but  The variable rule 

\begin{table}[!htp]
    \begin{tabularx}{\linewidth}{YY}
        \prfbyaxiom{Unit}
            {\Gamma \vdash \Unit : 1} &
        \prfbyaxiom{Variable}
            {\Gamma, x : \tau \vdash x : \tau} \\[1em]
        \prftree[r]{Abstraction}
            {\Gamma, }
            {\IsTerm{\Lam{x}{m}}} &
        \prftree[r]{Application}
            {\IsTerm{m_1}}
            {\IsTerm{m_2}}
            {\IsTerm{\App{m_1}{m_2}}} \\[1em]
        \prftree[r]{Let}
            {\IsVar{x}}
            {\IsTerm{e_0}}
            {\IsTerm{e_1}}
            {\IsTerm{\Let{x}{e_0}{e_1}}} &
        \prftree[r]{Fix}
            {\IsVar{f}}
            {\IsVar{x}}
            {\IsTerm{e}}
            {\IsTerm{\Fix{f}{x}{e}}}
    \end{tabularx}
\end{table}

\section{Extension for \cands}

Eventually, we would like to extend \AlgoM{} to cover the typing system used in \cands.  Let's first define the syntax of the language:

\begin{table}[!htp]
    \begin{tabularx}{\linewidth}{YY}
        \prftree[r]{Feature}
            {\IsFeature{f}}
            {\IsTerm{f}} &
        \prftree[r]{Usize}
            {\IsUsize{x}}
            {\IsTerm{x}} \\[1em]
        \prftree[r]{}
    \end{tabularx}
\end{table}

where \( \IsFeature{f} \) is true iff \( f \) matches the regex \code{"[a-zA-Z0-9*=']+"}, and \( \IsUsize{x} \) is true iff \( f \) is an unsigned 64-bit integer.

\cands{} has the following types:

Here are the typing rules:

\subsection{Vector and sets}

\end{document}
