//! Type unification.

use crate::syntax::{
    TypeVar, Type, TypeScheme,
};

use std::{
    collections::{ HashMap },
    fmt
};



/// A substitution.
/// 
/// A substitution $ [ \tau_i / \alpha_i \mid 1 \leq i \leq n ] $ substitutes
/// type $ \tau_i $ for type variable $ \alpha_i $.  It is defined by a map
/// from type variables to types.
/// 
/// A substitution can be applied to types ([`crate::syntax::Type::apply`])
/// and, by extension, to type schemes ([`crate::syntax::TypeScheme::apply`]).
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Substitution(HashMap<TypeVar, Type>);



impl Substitution {
    /// Create an empty / trivial / identity / whatever-you-call-it
    /// substitution $ [] $.
    pub fn new() -> Self {
        Self(HashMap::new())
    }

    /// Updates a substitution $ S $ with a new mapping $ [ \tau / \alpha ] $.
    /// 
    /// This returns a substitution $ S' = [ \tau_i / \alpha_i \mid 1 \leq i
    /// \leq m ] $ such that, for every $ 1 \leq i \leq m $:
    /// *   If $ \alpha_i = \alpha $, then $ \tau_i = \tau $.
    /// *   Otherwise, $ \tau_i = S \alpha_i $.
    pub fn update(mut self, k: TypeVar, v: Type) -> Self {
        self.0.insert(k, v);
        self
    }

    /// Create a substitution that contains just one mapping: $ [ \tau /
    /// \alpha ] $.
    pub fn singleton(k: TypeVar, v: Type) -> Self {
        Self::new().update(k, v)
    }

    /// Get the (reference to the) type which the given type variable maps to,
    /// if it exists.
    pub fn get(&self, k: &TypeVar) -> Option<&Type> {
        self.0.get(k)
    }

    /// Compose this substitution $ S $ with another substitution $ R $.
    /// 
    /// The composition $ RS $ is defined by $ [ R (S \alpha) / \alpha \mid \alpha \in \mathrm{dom} (S) ] \cup [ R \alpha / \alpha \mid \alpha \in \mathrm{dom} (R) \setminus \mathrm{dom} (S) ] $.
    pub fn compose(self, other: Substitution) -> Substitution {
        // Use this impl if this is provably correct
        // self.0.into_iter()
        //     .fold(
        //         other,
        //         |other, (k, v)| {
        //             let v = v.apply(&other);
        //             other.update(k, v)
        //         }
        //     )
        self.0.into_iter()
            .fold(
                other.clone(),
                |composition, (k, v)| {
                    let v = v.apply(&other);
                    composition.update(k, v)
                }
            )
    }
}



impl fmt::Display for Substitution {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (k, v) in &self.0 {
            write!(f, "{} -> {}, ", k, v)?;
        }

        Ok(())
    }
}



/// Find the most general unifier of the two given types if possible.
/// 
/// A *unifier* $ S $ of types $ \tau_1, \tau_2 $ is a substitution such that
/// $ S \tau_1 = S \tau_2 $.  A *most general unifier* (mgu) of $ \tau_1,
/// \tau_2 $ is their unifier $ S $ such that for any other unifier $ S' $,
/// there exists a substitution $ R $ such that $ S' = SR $.
/// 
/// For example, consider the types $ A \rightarrow B $ and $ C $, where $ A,
/// B, C $ are type variables.  The most general unifier $ S $ is $ [ A
/// \rightarrow B / C ] $.  There is another unifier $ S' = [ 1 \rightarrow B /
/// C ] $, where $ 1 $ is the unit type.  We can find a substitution $ R = [ 1
/// / A ] $ which satisfies $ S' = SR $.
pub fn mgu(ty1: Type, ty2: Type) -> Option<Substitution> {
    eprintln!("Trying to unify `{}` with `{}`...", ty1, ty2);

    let sub = loop {
        match (ty1, ty2) {
            (Type::Var { var: ref var1 }, ty2) => {
                if let Type::Var { var: ref var2 } = ty2 {
                    if var1 == var2 {
                        break Some(Substitution::new());
                    }
                }

                if !var1.is_free_in(&ty2) {
                    break Some(Substitution::singleton(var1.clone(), ty2.clone()));
                }

                break None;
            },

            (Type::Func { f: f1, tys: tys1 }, Type::Func { f: f2, tys: tys2 }) => {
                if (f1 != f2) || (tys1.len() != tys2.len()) {
                    break None;
                }

                break tys1.into_iter()
                    .zip(tys2.into_iter())
                    .fold(
                        Some(Substitution::new()),
                        |sub, (ty1, ty2)| {
                            sub.and_then(|sub| {
                                let ty1 = ty1.apply(&sub);
                                let ty2 = ty2.apply(&sub);
                                mgu(ty1, ty2).map(move |newsub| sub.compose(newsub))
                            })
                        }
                    );
            },

            (ty1, Type::Var { var: var2 }) => {
                if !var2.is_free_in(&ty1) {
                    break Some(Substitution::singleton(var2.clone(), ty1.clone()));
                }

                break None;
            },
        }

        unreachable!()
    };

    if let Some(ref sub) = sub {
        eprintln!("Substitution is {}", sub);
    }

    sub
}
