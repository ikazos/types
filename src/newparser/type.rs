use crate::{
    syntax::{
        TypeVar, Type,
    },
    lexer::{
        TypeToken, TypeTokens,
    },
    newparser::{
        match_tokens,
    },
};



use nom::{
    IResult,
    branch::{ alt },
    bytes::complete::{ take },
    combinator::{ map, map_res },
    multi::{ separated_list0 },
};



pub fn typevar(tokens: TypeTokens) -> IResult<TypeTokens, TypeVar> {
    map_res(
        take(1usize),
        |tokens: TypeTokens| {
            match &tokens.0[..] {
                [ TypeToken::Id(id) ] =>
                    Ok(TypeVar::from_string(id.to_owned())),

                _ => Err(()),
            }
        }
    )(tokens)
}



pub fn var(tokens: TypeTokens) -> IResult<TypeTokens, Type> {
    map(
        typevar,
        |tv| Type::Var { var: tv }
    )(tokens)
}



pub fn func(tokens: TypeTokens) -> IResult<TypeTokens, Type> {
    let (tokens, f) = 
        map_res(
            take(1usize),
            |tokens: TypeTokens| {
                match &tokens.0[..] {
                    [ TypeToken::Id(id) ] =>
                        Ok(id.to_owned()),

                    _ => Err(()),
                }
            }
        )(tokens)?;

    let (tokens, _) = match_tokens(&[ TypeToken::LeftParen ])(tokens)?;
    let (tokens, tys) = separated_list0(
        match_tokens(&[ TypeToken::Comma ]),
        r#type
    )(tokens)?;
    let (tokens, _) = match_tokens(&[ TypeToken::RightParen ])(tokens)?;

    Ok((tokens, Type::Func { f, tys }))
}



pub fn r#type(tokens: TypeTokens) -> IResult<TypeTokens, Type> {
    alt((
        func, var
    ))(tokens)
}