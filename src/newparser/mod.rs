mod term;
mod r#type;



use crate::{
    syntax::{ Term, Type },
    lexer::{ TermToken, TermTokens, TypeToken, TypeTokens },
};



use logos::Logos;

use nom::{
    IResult,
    InputIter, InputTake,
    combinator::{ map_res },
    bytes::complete::{ take },
};



/// Parser that matches token slices.
fn match_tokens<'a, I, E, T>(token_slice: &'a [T]) -> impl FnMut(I) -> IResult<I, (), E> + 'a
    where   T: PartialEq,
            I: AsRef<[T]> + InputIter + InputTake + Clone + 'a,
            E: nom::error::FromExternalError<I, ()> + nom::error::ParseError<I> + 'a,
{
    let token_slice_len = token_slice.len();

    map_res(
        take(token_slice_len),
        move |tokens: I| {
            match tokens.as_ref() == token_slice {
                true => Ok(()),
                false => Err(()),
            }
        }
    )
}



#[derive(Debug, Clone)]
pub enum Parse<T> {
    Success(T),
    Maybe,
    Fail,
}



pub fn parse_term(s: &str) -> Parse<Term> {
    //  Lex the input.
    let real_tokens: Vec<TermToken> = TermToken::lexer(s).collect();
    let tokens = TermTokens(&real_tokens);

    //  Parse a term.
    let (tokens, parse) =
        match self::term::term(tokens) {
            Ok((tokens, term)) => (tokens, Parse::Success(term)),
            Err(nom::Err::Incomplete(_)) => {
                return Parse::Maybe;
            },
            _ => {
                eprintln!("Error: failed to parse type.");
                return Parse::Fail;
            },
        };

    match tokens.0[..] == [ TermToken::Semicolon ] {
        true => parse,
        false => {
            eprintln!("Error: trailing tokens.");
            Parse::Fail
        },
    }
}



pub fn parse_type(s: &str) -> Parse<Type> {
    //  Lex the input.
    let real_tokens: Vec<TypeToken> = TypeToken::lexer(s).collect();
    let tokens = TypeTokens(&real_tokens);

    //  Parse a term.
    let (tokens, parse) =
        match self::r#type::r#type(tokens) {
            Ok((tokens, ty)) => (tokens, Parse::Success(ty)),
            Err(nom::Err::Incomplete(_)) => {
                return Parse::Maybe;
            },
            _ => {
                eprintln!("Error: failed to parse type.");
                return Parse::Fail;
            },
        };

    match tokens.0[..] == [ TypeToken::Semicolon ] {
        true => parse,
        false => {
            eprintln!("Error: trailing tokens.");
            Parse::Fail
        },
    }
}



#[test]
fn mytest() {
    eprintln!("{:?}",
        parse_term("<>;")
    );
}