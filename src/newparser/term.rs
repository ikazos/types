use crate::{
    syntax::{
        TermVar, Term
    },
    lexer::{
        TermToken, TermTokens,
    },
    newparser::{
        match_tokens,
    },
};



use nom::{
    IResult,
    branch::{ alt },
    bytes::complete::{ take },
    combinator::{ map, map_res },
    multi::many0,
};



pub fn termvar(tokens: TermTokens) -> IResult<TermTokens, TermVar> {
    map_res(
        take(1usize),
        |tokens: TermTokens| {
            match &tokens.0[..] {
                [ TermToken::Id(id) ] =>
                    Ok(TermVar::from_string(id.to_owned())),

                _ => Err(()),
            }
        }
    )(tokens)
}



pub fn r#const(tokens: TermTokens) -> IResult<TermTokens, Term> {
    map(
        match_tokens(&[ TermToken::Const ]),
        |_| Term::Const
    )(tokens)
}



pub fn abs(tokens: TermTokens) -> IResult<TermTokens, Term> {
    let (tokens, _) = match_tokens(&[ TermToken::Fn ])(tokens)?;
    let (tokens, x) = termvar(tokens)?;
    let (tokens, _) = match_tokens(&[ TermToken::Arrow ])(tokens)?;
    let (tokens, e) = term(tokens)?;

    Ok((tokens, Term::Abs { x, e: Box::new(e) }))
}



pub fn parens(tokens: TermTokens) -> IResult<TermTokens, Term> {
    let (tokens, _) = match_tokens(&[ TermToken::LeftParen ])(tokens)?;
    let (tokens, term) = term(tokens)?;
    let (tokens, _) = match_tokens(&[ TermToken::RightParen ])(tokens)?;

    Ok((tokens, term))
}



pub fn var(tokens: TermTokens) -> IResult<TermTokens, Term> {
    map(
        termvar,
        |termvar| Term::Var { var: termvar }
    )(tokens)
}



pub fn r#let(tokens: TermTokens) -> IResult<TermTokens, Term> {
    let (tokens, _) = match_tokens(&[ TermToken::Let ])(tokens)?;
    let (tokens, x) = termvar(tokens)?;
    let (tokens, _) = match_tokens(&[ TermToken::Equals ])(tokens)?;
    let (tokens, ev) = term(tokens)?;
    let (tokens, _) = match_tokens(&[ TermToken::In ])(tokens)?;
    let (tokens, e) = term(tokens)?;

    Ok((tokens, Term::Let { x, ev: Box::new(ev), e: Box::new(e) }))
}



pub fn fix(tokens: TermTokens) -> IResult<TermTokens, Term> {
    let (tokens, _) = match_tokens(&[ TermToken::Fun ])(tokens)?;
    let (tokens, f) = termvar(tokens)?;
    let (tokens, x) = termvar(tokens)?;
    let (tokens, _) = match_tokens(&[ TermToken::Equals ])(tokens)?;
    let (tokens, e) = term(tokens)?;

    Ok((tokens, Term::Fix { f, x, e: Box::new(e) }))
}



pub fn oneterm(tokens: TermTokens) -> IResult<TermTokens, Term> {
    alt((
        r#const, abs, r#let, fix, parens, var
    ))(tokens)
}



pub fn term(tokens: TermTokens) -> IResult<TermTokens, Term> {
    let (tokens, term0) = oneterm(tokens)?;
    let (tokens, terms) = many0(oneterm)(tokens)?;

    let term = terms.into_iter()
        .fold(
            term0,
            |term1, term2| {
                Term::App { e1: Box::new(term1), e2: Box::new(term2) }
            }
        );

    Ok((tokens, term))
}