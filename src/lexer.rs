use logos::{ Logos };

use nom::{ InputIter, InputLength, InputTake };



#[derive(Debug, Logos, PartialEq)]
pub enum TermToken {
    #[token("fn")]
    Fn,

    #[token("let")]
    Let,

    #[token("in")]
    In,

    #[token("fun")]
    Fun,

    #[token("=>")]
    Arrow,

    #[token("=")]
    Equals,

    #[token("(")]
    LeftParen,

    #[token(")")]
    RightParen,

    #[token("<>")]
    Const,

    #[token(";")]
    Semicolon,

    #[regex("[a-zA-Z_]+[a-zA-Z0-9_]*", |lex| lex.slice().to_owned())]
    Id(String),

    #[error]
    #[regex(r"[ \t\n\f]+", logos::skip)]
    Error,
}



#[derive(Debug, Copy, Clone)]
pub struct TermTokens<'a>(pub &'a [TermToken]);



impl<'a> InputIter for TermTokens<'a> {
    type Item = &'a TermToken;
    type Iter = std::iter::Enumerate<std::slice::Iter<'a, TermToken>>;
    type IterElem = std::slice::Iter<'a, TermToken>;

    fn iter_indices(&self) -> Self::Iter {
        self.0.iter().enumerate()
    }

    fn iter_elements(&self) -> Self::IterElem {
        self.0.iter()
    }

    fn position<P>(&self, predicate: P) -> Option<usize>
        where P: Fn(Self::Item) -> bool
    {
        self.0.iter().position(|elem| predicate(elem))
    }

    fn slice_index(&self, count: usize) -> Result<usize, nom::Needed> {
        if self.0.len() >= count {
            Ok(count)
        }
        else {
            Err(nom::Needed::new(count - self.0.len()))
        }
    }
}



impl<'a> InputTake for TermTokens<'a> {
    fn take(&self, count: usize) -> Self {
        Self(&self.0[0..count])
    }

    fn take_split(&self, count: usize) -> (Self, Self) {
        let (prefix, suffix) = self.0.split_at(count);
        (Self(suffix), Self(prefix))
    }
}



impl<'a> InputLength for TermTokens<'a> {
    fn input_len(&self) -> usize {
        self.0.len()
    }
}



impl<'a> AsRef<[TermToken]> for TermTokens<'a> {
    fn as_ref(&self) -> &[TermToken] {
        &self.0
    }
}



#[derive(Debug, Logos, PartialEq)]
pub enum TypeToken {
    #[token("(")]
    LeftParen,

    #[token(")")]
    RightParen,

    #[token(",")]
    Comma,

    #[token(";")]
    Semicolon,

    #[regex("[a-zA-Z_]+[a-zA-Z0-9_]*", |lex| lex.slice().to_owned())]
    Id(String),

    #[error]
    #[regex(r"[ \t\n\f]+", logos::skip)]
    Error,
}



#[derive(Debug, Copy, Clone)]
pub struct TypeTokens<'a>(pub &'a [TypeToken]);



impl<'a> InputIter for TypeTokens<'a> {
    type Item = &'a TypeToken;
    type Iter = std::iter::Enumerate<std::slice::Iter<'a, TypeToken>>;
    type IterElem = std::slice::Iter<'a, TypeToken>;

    fn iter_indices(&self) -> Self::Iter {
        self.0.iter().enumerate()
    }

    fn iter_elements(&self) -> Self::IterElem {
        self.0.iter()
    }

    fn position<P>(&self, predicate: P) -> Option<usize>
        where P: Fn(Self::Item) -> bool
    {
        self.0.iter().position(|elem| predicate(elem))
    }

    fn slice_index(&self, count: usize) -> Result<usize, nom::Needed> {
        if self.0.len() >= count {
            Ok(count)
        }
        else {
            Err(nom::Needed::new(count - self.0.len()))
        }
    }
}



impl<'a> InputTake for TypeTokens<'a> {
    fn take(&self, count: usize) -> Self {
        Self(&self.0[0..count])
    }

    fn take_split(&self, count: usize) -> (Self, Self) {
        let (prefix, suffix) = self.0.split_at(count);
        (Self(suffix), Self(prefix))
    }
}



impl<'a> InputLength for TypeTokens<'a> {
    fn input_len(&self) -> usize {
        self.0.len()
    }
}



impl<'a> AsRef<[TypeToken]> for TypeTokens<'a> {
    fn as_ref(&self) -> &[TypeToken] {
        &self.0
    }
}