mod syntax;
mod lexer;
mod parser;
mod newparser;
mod unif;
mod infer;



use crate::{
    syntax::{ Type },
    // parser::{ Parse },
    newparser::{ Parse },
};

use std::io::{ self, Write };

use structopt::StructOpt;



#[derive(Debug, StructOpt)]
#[structopt(name = "types", about = "Type inference")]
struct Args {
    /// Do type unification instead of inference
    #[structopt(short, long)]
    unify: bool,
}



macro_rules! flush {
    () => { io::stdout().flush().unwrap() };
}



fn get<T, F: Fn(&str) -> Parse<T>>(f: F) -> Option<T> {
    let mut input = String::new();
    let mut first_loopiter = true;

    loop {
        if first_loopiter {
            print!(">>> ");
        }
        else {
            print!("... ");
        }

        flush!();

        let mut buffer = String::new();
        io::stdin().read_line(&mut buffer).unwrap();

        if buffer.trim().is_empty() {
            return None;
        }

        input.extend(buffer.chars());

        match f(&input) {
            Parse::Success(t) => {
                return Some(t);
            },

            Parse::Maybe => {},
            Parse::Fail => {
                return None;
            },
        };
        
        first_loopiter = false;
    }
}



fn find_mgu() {
    loop {
        println!("Type 1:");
        let ty1 = match get(newparser::parse_type) {
            Some(ty) => ty,
            None => {
                println!("Starting over...");
                continue;
            },
        };
        println!("Type 1 is {}", ty1);

        println!("Type 2:");
        let ty2 = match get(newparser::parse_type) {
            Some(ty) => ty,
            None => {
                println!("Starting over...");
                continue;
            },
        };
        println!("Type 2 is {}", ty2);

        println!("Computing most general unifier...");
        match unif::mgu(ty1, ty2) {
            Some(mgu) => {
                println!("Most general unifier: {}", mgu);
            },

            None => {
                println!("These types cannot be unified.");
            },
        };

        println!("");
    }
}



fn term_loop() {
    loop {
        println!("Term:");
        let term = match get(newparser::parse_term) {
            Some(term) => term,
            None => {
                println!("Starting over...");
                continue;
            },
        };
        println!("Term is {}", term);

        println!("Inferring type for term...");
        match infer::infer(term) {
            Some(ty) => {
                println!("Type: {}", ty);
            },

            None => {
                println!("Cannot infer type.");
            },
        };

        println!("");
    }
}



fn main() {
    let args = Args::from_args();

    if args.unify {
        find_mgu();
    }
    else {
        term_loop();
    }
}