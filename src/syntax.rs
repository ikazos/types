//! The syntax of our programming language.



use crate::unif::Substitution;



use std::{ fmt, iter, collections::HashSet };

//  https://stackoverflow.com/a/27826181
use std::sync::atomic::{ AtomicUsize, Ordering };



static TERM_VAR_COUNTER: AtomicUsize = AtomicUsize::new(0);
static TYPE_VAR_COUNTER: AtomicUsize = AtomicUsize::new(0);



/// A term variable.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct TermVar(String);



impl TermVar {
    /// Create a globally new term variable with the label `Sx`, where `S` is
    /// the given string and `x` is some non-negative integer.
    pub fn new<S: AsRef<str>>(s: S) -> Self {
        Self(format!("{}{}", s.as_ref(), TERM_VAR_COUNTER.fetch_add(1, Ordering::Relaxed)))
    }

    /// Create a term variable with the given label.
    /// 
    /// **Note: this term variable is not guaranteed to be globally new.**
    pub fn from_string(s: String) -> Self {
        Self(s.to_owned())
    }
}



impl fmt::Display for TermVar {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}



/// A term.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Term {
    /// A constant (unit).
    Const,

    /// A variable.
    Var {
        /// The variable.
        var: TermVar
    },

    /// A lambda abstraction.
    Abs {
        /// The variable.
        x: TermVar,
        
        /// The `function body'.
        e: Box<Term>
    },

    /// A function application.
    App {
        /// The function.
        e1: Box<Term>,
        
        /// The argument.
        e2: Box<Term>
    },

    /// A let-binding.
    Let {
        /// The variable.
        x: TermVar,
        
        /// The term the variable represents.
        ev: Box<Term>,
        
        /// The body of the let-binding.
        e: Box<Term>
    },

    /// A recursive function.
    Fix {
        /// The variable that represents the function.
        f: TermVar,
        
        /// The variable that represents the argument.
        x: TermVar,
        
        /// The function body.
        e: Box<Term>
    },
}



impl fmt::Display for Term {
    fn fmt(&self, fmter: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Term::Const => write!(fmter, "<>"),
            Term::Var { var } => write!(fmter, "{}", var),
            Term::Abs { x, e } => write!(fmter, "fn {} => {}", x, e),
            Term::App { e1, e2 } => write!(fmter, "({}) {}", e1, e2),
            Term::Let { x, ev, e } => write!(fmter, "let {} = {} in {}", x, ev, e),
            Term::Fix { f, x, e } => write!(fmter, "fun {} {} = {}", f, x, e),
        }
    }
}



/// A type variable.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct TypeVar(String);



impl TypeVar {
    /// Create a globally new type variable with the label `Sx`, where `S` is
    /// the given string and `x` is some non-negative integer.
    pub fn new<S: AsRef<str>>(s: S) -> Self {
        Self(format!("{}{}", s.as_ref(), TYPE_VAR_COUNTER.fetch_add(1, Ordering::Relaxed)))
    }

    /// Create a type variable with the given label.
    /// 
    /// **Note: this type variable is not guaranteed to be globally new.**
    pub fn from_string(s: String) -> Self {
        Self(s.to_owned())
    }

    /// Return `true` iff this type variable is free in (i.e. appears in) the
    /// given type.
    pub fn is_free_in(&self, ty: &Type) -> bool {
        match ty {
            Type::Var { var } =>
                var == self,

            Type::Func { tys, .. } =>
                tys.iter()
                    .any(|ty| self.is_free_in(ty)),
        }
    }
}



impl fmt::Display for TypeVar {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}



/// A type.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Type {
    /// If $ \alpha $ is a type variable, then $ \alpha $ is a type.
    Var { var: TypeVar },

    /// If $ f $ is an identifier, and $ \tau_1, \cdots, \tau_n $ are types,
    /// then $ f ( \tau_1, \cdots, \tau_n ) $ is a type.
    Func { f: String, tys: Vec<Type> },
}



impl Type {
    /// Apply a substitution to this type.
    ///
    /// Let $ S = \{ \tau_1/\alpha_1, \cdots, \tau_n/\alpha_n \} $ be a
    /// substitution, and $ \sigma $ be a type. Then $ S \sigma $ is defined
    /// as follows:
    ///
    /// *   If $ \sigma = \beta $, a type variable,
    ///     *   If $ \tau/\beta $ is in $ S $ ($ S $ maps $ \beta $ to $ \tau
    ///         $), then $ S \sigma = \tau $.
    ///     *   Otherwise, $ S \sigma = \sigma $.
    /// *   If $ \sigma = f ( \sigma_1, \cdots, \sigma_m ) $, then $ S \sigma =
    ///     f ( S \sigma_1, \cdots, S \sigma_m ) $.
    pub fn apply(self, sub: &Substitution) -> Self {
        match self {
            Type::Var { var } => {
                match sub.get(&var) {
                    Some(ty) => ty.clone(),
                    None => Type::Var { var },
                }
            },

            Type::Func { f, tys } => {
                let tys = tys.into_iter()
                    .map(|ty| ty.apply(sub))
                    .collect();

                Type::Func { f, tys }
            },
        }
    }

    /// Get the set of free type variables in this type.  Since there is no
    /// variable-binding in the syntax of types, this is just the set of all
    /// type variables in this type.
    ///
    /// Let $ \tau $ be a type.  Then:
    ///
    /// *   If $ \tau = \alpha $, a type variable, then $ ftv (\tau) = \{
    ///     \alpha \} $.
    /// *   If $ \tau = f ( \tau_1, \cdots, \tau_n ) $, then $ ftv (\tau) =
    ///     \bigcup_{i=1}^n ftv (\tau_i) $.
    pub fn get_free_type_variables(&self) -> HashSet<TypeVar> {
        match self {
            Type::Var { var } => {
                let mut set = HashSet::new();
                set.insert(var.clone());
                set
            },

            Type::Func { tys, .. } => {
                tys.iter()
                    .map(Self::get_free_type_variables)
                    .fold(
                        HashSet::new(),
                        |mut set1, set2| {
                            set1.extend(set2.into_iter());
                            set1
                        }
                    )
            },
        }
    }
}



impl fmt::Display for Type {
    fn fmt(&self, fmter: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Type::Var { var } => write!(fmter, "{}", var),
            Type::Func { f, tys } => {
                write!(
                    fmter,
                    "{} ({})",
                    f,
                    tys.iter()
                        .map(|ty| ty.to_string())
                        .reduce(|a, b| format!("{}, {}", a, b))
                        .unwrap_or_else(|| format!(""))
                )
            },
        }
    }
}



/// A type scheme.
///
/// If $ \tau $ is a type, and $ \alpha_1, \cdots, \alpha_n $ (where $ n \geq
/// 0 $) are type variables, then $ \forall \alpha_1. \cdots \forall
/// \alpha_n. \tau $ is a type scheme.
///
/// In the Algorithm M paper, this is written as $ \forall \vec{\alpha} .
/// \tau $; consider $ \vec{\alpha} $ as a vector (list) of type variables.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct TypeScheme {
    /// The $ \forall \alpha_1. \cdots \forall \alpha_n. $ part.
    pub vars: Vec<TypeVar>,
    /// The $ \tau $ part.
    pub ty: Type,
}



impl TypeScheme {
    /// Apply a substitution to this type scheme.
    ///
    /// Let $ S $ be a substitution, and $ \forall \vec{\alpha} . \tau $ a
    /// type scheme.  Then $ S (\forall \vec{\alpha} . \tau) = \forall
    /// \vec{\beta} . S [ \vec{\beta} / \vec{\alpha} ] \tau $, where $
    /// \vec{\beta} $ are globally new type variables.
    pub fn apply(self, sub: &Substitution) -> Self {
        let Self { vars, ty } = self;

        let newvars: Vec<TypeVar> = iter::repeat_with(|| TypeVar::new("B"))
            .take(vars.len())
            .collect();

        let varssub = newvars.iter()
            .enumerate()
            .fold(
                Substitution::new(),
                |sub, (k, newvar)| sub.update(vars[k].clone(), Type::Var { var: newvar.clone() })
            );

        let ty = ty.apply(&varssub).apply(sub);

        Self { vars: newvars, ty }
    }

    /// Get the set of free type variables in this type scheme.
    ///
    /// Let $ \sigma = \forall \vec{\alpha} . \tau $ be a type scheme.  Then $
    /// ftv (\sigma) = ftv (\tau) \setminus \vec{\alpha} $, where $ ftv (\tau)
    /// $ is the set of free type variables in the type $ \tau $.
    pub fn get_free_type_variables(&self) -> HashSet<TypeVar> {
        self.ty.get_free_type_variables()
            .into_iter()
            .filter(|ftv| !self.vars.contains(ftv))
            .collect()
    }
}



impl From<Type> for TypeScheme {
    fn from(ty: Type) -> Self {
        Self {
            vars: vec![],
            ty,
        }
    }
}



impl fmt::Display for TypeScheme {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}{}",
            self.vars.iter()
                .map(|var| format!("∀{}.", var))
				.collect::<String>(),
            self.ty
        )
    }
}
