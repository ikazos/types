//! Type inference with Algorithm M.

use crate::syntax::{
    TermVar, Term,
    TypeVar, Type, TypeScheme
};

use crate::unif::{ mgu, Substitution };

use std::{
    collections::{ HashMap, HashSet },
    fmt, iter,
};



/// Macro to create a "constant" type, i.e. a 0-ary function type with function
/// identifier `Const`.
macro_rules! consty {
    () => { Type::Func { f: format!("Const"), tys: vec![] } };
}

/// Macro to create a "function" type, i.e. a 2-ary function type with function
/// identifier `Func`.
macro_rules! functy {
    ($t1:expr, $t2:expr) => { Type::Func { f: format!("Func"), tys: vec![ $t1, $t2 ] } };
}



/// A type context.
/// 
/// A map $ [ \sigma_i / x_i \mid 1 \leq i \leq n ] $ from term variables $ x_i
/// $ to type schemes $ \sigma_i $.
#[derive(Debug, Clone, PartialEq, Eq)]
struct TypeContext(HashMap<TermVar, TypeScheme>);



impl TypeContext {
    /// Create an empty / trivial / identity / whatever-you-call-it type
    /// context $ [] $.
    pub fn new() -> Self {
        Self(HashMap::new())
    }

    /// Updates a type context $ \Gamma $ with a new mapping $ [ \sigma / x ] $.
    /// 
    /// This returns a type context $ \Gamma' = [ \sigma_i / x_i \mid
    /// 1 \leq i \leq m ] $ such that, for every $ 1 \leq i \leq m
    /// $:
    /// *   If $ x_i = x $, then $ \sigma_i = \sigma $.
    /// *   Otherwise, $ \sigma_i = \Gamma (x_i) $.
    pub fn update(mut self, k: TermVar, v: TypeScheme) -> Self {
        self.0.insert(k, v);
        self
    }

    /// Create a singleton type context that just contains one mapping: $ [
    /// \sigma / x ] $.
    pub fn singleton(k: TermVar, v: TypeScheme) -> Self {
        Self::new().update(k, v)
    }

    /// Get the (reference to the) type scheme which the given term variable
    /// maps to, if it exists.
    pub fn get(&self, k: &TermVar) -> Option<&TypeScheme> {
        self.0.get(k)
    }

    /// Apply a substitution $ S $ to this type context.
    /// 
    /// If $ \Gamma = [ \sigma_i / x_i \mid 1 \leq i \leq n ] $, then $ S
    /// \Gamma = [ S \sigma_i / x_i \mid 1 \leq i \leq n ] $.
    pub fn apply(self, sub: &Substitution) -> Self {
        Self(
            self.0.into_iter()
                .map(|(k, v)| (k, v.apply(sub)))
                .collect()
        )
    }

    /// Return the set of the free type variables in this type context.
    /// 
    /// If $ \Gamma = [ \sigma_i / x_i \mid 1 \leq i \leq n ] $, then $ ftv (
    /// \Gamma ) = \bigcup_{1 \leq i \leq n} ftv ( \sigma_i ) $.
    /// 
    /// For $ ftv ( \sigma ) $, see [`crate::syntax::TypeScheme::get_free_type_variables`].
    pub fn get_free_type_variables(&self) -> HashSet<TypeVar> {
        self.0.values()
            .map(|tysch| tysch.get_free_type_variables())
            .fold(
                HashSet::new(),
                |mut set1, set2| {
                    set1.extend(set2.into_iter());
                    set1
                }
            )
    }

    /// From Lee & Yi (1998): $ Clos_\Gamma ( \tau ) = \forall \vec{\alpha} .
    /// \tau $, where $ \vec{\alpha} = ftv ( \tau ) \setminus ftv ( \Gamma ) $.
    pub fn closure(&self, ty: Type) -> TypeScheme {
        let ty_ftvs = ty.get_free_type_variables();
		eprintln!("Closure: free type vars of type `{}` is:", ty);
		eprintln!("         {}", ty_ftvs.iter().map(|ftv| format!("{}", ftv)).reduce(|a, b| format!("{}, {}", a, b)).unwrap_or_else(|| format!("")));
        let ctx_ftvs = self.get_free_type_variables();
		eprintln!("Closure: free type vars of context `{}` is:", self);
		eprintln!("         {}", ctx_ftvs.iter().map(|ftv| format!("{}", ftv)).reduce(|a, b| format!("{}, {}", a, b)).unwrap_or_else(|| format!("")));

        let vars: Vec<TypeVar> = ty_ftvs.into_iter()
            .filter(|var| !ctx_ftvs.contains(var))
            .collect();

		eprintln!("Closure: the remaining vars are:");
		eprintln!("         {}", vars.iter().map(|var| format!("{}", var)).reduce(|a, b| format!("{}, {}", a, b)).unwrap_or_else(|| format!("")));

        TypeScheme { vars, ty }
    }
}



impl fmt::Display for TypeContext {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (k, v) in &self.0 {
            write!(f, "{} -> {}, ", k, v)?;
        }

        Ok(())
    }
}



/// Algorithm M.
/// 
/// Given a type context $ \Gamma $, a term $ m $ and a type constraint $ \tau
/// $, this function returns a substitution $ S $ such that the inferred type
/// of $ m $ is $ S \tau $, if a type can be inferred for $ m $.
fn algom(ctx: TypeContext, term: Term, rho: Type) -> Option<Substitution> {
    eprintln!("{}", iter::repeat(" ").take(80).collect::<String>());
    eprintln!("Type context is {}", ctx);
    eprintln!("Trying to infer `{}` as type `{}`...", term, rho);

    let sub = match term {
        Term::Const =>
            mgu(rho, consty!()),

        Term::Var { var } => {
            let tysch = ctx.get(&var)?;

            let newty = tysch.clone().apply(&Substitution::new()).ty;

            mgu(rho, newty)
        },

        Term::Abs { x, e } => {
            let b1 = Type::Var { var: TypeVar::new("B") };
            let b2 = Type::Var { var: TypeVar::new("B") };
            let b1_to_b2 = functy!( b1.clone(), b2.clone() );
            let sub1 = mgu(rho, b1_to_b2)?;

            let sub2 = algom(
                ctx.apply(&sub1).update(x, TypeScheme::from(b1).apply(&sub1)),
                *e,
                b2.apply(&sub1)
            )?;

            Some(sub1.compose(sub2))
        },

        Term::App { e1, e2 } => {
            let b = Type::Var { var: TypeVar::new("B") };
            let b_to_rho = functy!( b.clone(), rho );
            let sub1 = algom(ctx.clone(), *e1, b_to_rho)?;

            let sub2 = algom(ctx.apply(&sub1), *e2, b.apply(&sub1))?;

            Some(sub1.compose(sub2))
        },

        Term::Let { x, ev, e } => {
            let b = Type::Var { var: TypeVar::new("B") };
            let sub1 = algom(ctx.clone(), *ev, b.clone())?;

            let sub1_ctx = ctx.apply(&sub1);
            let tysch = sub1_ctx.closure(b.apply(&sub1));

            let sub2 = algom(
                sub1_ctx.update(x, tysch),
                *e,
                rho.apply(&sub1)
            )?;

            Some(sub1.compose(sub2))
        },

        Term::Fix { f, x, e } => {
            algom(
                ctx.update(f, TypeScheme::from(rho.clone())),
                Term::Abs { x, e },
                rho
            )
        },
    };

    if let Some(ref sub) = sub {
        eprintln!("Substitution is {}", sub);
    }
    eprintln!("{}", iter::repeat(" ").take(80).collect::<String>());

    sub
}



/// Infer a type for the given term, if possible.
/// 
/// This runs Algorithm M with the type constraint that the term must have type
/// $ A $, which is a globally new type variable.  If Algorithm M is
/// successful, it returns a substitution $ S $.  This function then starts
/// with the type (variable) $ A $, and keeps applying $ S $ to this type until
/// the application of the substitution no longer changes the type.  The final
/// type is the type for the given term.
pub fn infer(term: Term) -> Option<Type> {
    let mut ty = Type::Var { var: TypeVar::new("A") };

    let sub = algom(
        TypeContext::new(),
        term,
        ty.clone(),
    );

    sub.map(|sub| {
        eprintln!("Correct substitution: {}", sub);

        let mut old_ty = ty.clone();
        let mut first_loopiter = true;

        while first_loopiter || (old_ty != ty) {
            old_ty = ty.clone();
            ty = ty.apply(&sub);
            first_loopiter = false;
        }

        ty
    })
}
