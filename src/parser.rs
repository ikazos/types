mod internal {
    use crate::syntax::{
        TermVar, Term,
        TypeVar, Type,
    };



    use nom::{
        IResult,
        branch::{ alt },
        bytes::streaming::{ tag },
        character::{
            complete::{ multispace0 },
            streaming::{ alpha1, alphanumeric0 },
        },
        combinator::{ fail, map, recognize },
        multi::{ many0, many1, separated_list0 },
        sequence::{ preceded },
    };



    pub fn parse_id(s: &str) -> IResult<&str, String> {
        let (s, id) =
            recognize(
                preceded(alpha1, alphanumeric0)
            )(s)?;

        if [ "fn", "let", "fun", "in" ].contains(&id) {
            let _: (&str, ()) = fail(s)?;
        }

        Ok((s, id.to_owned()))
    }



    pub fn parse_termvar(s: &str) -> IResult<&str, TermVar> {
        map(
            parse_id,
            |s| TermVar::from_string(s)
        )(s)
    }



    pub fn parse_typevar(s: &str) -> IResult<&str, TypeVar> {
        map(
            parse_id,
            |s| TypeVar::from_string(s)
        )(s)
    }



    pub fn parse_const(s: &str) -> IResult<&str, Term> {
        map(
            tag("<>"),
            |_| Term::Const
        )(s)
    }



    pub fn parse_var_as_term(s: &str) -> IResult<&str, Term> {
        map(
            parse_termvar,
            |tv| Term::Var { var: tv }
        )(s)
    }



    pub fn parse_abs(s: &str) -> IResult<&str, Term> {
        let (s, _) = tag("fn")(s)?;
        let (s, x) = preceded(multispace0, parse_termvar)(s)?;
        let (s, _) = preceded(multispace0, tag("=>"))(s)?;
        let (s, e) = preceded(multispace0, parse_term)(s)?;

        Ok((s, Term::Abs { x, e: Box::new(e) }))
    }



    pub fn parse_let(s: &str) -> IResult<&str, Term> {
        let (s, _) = tag("let")(s)?;
        let (s, x) = preceded(multispace0, parse_termvar)(s)?;
        let (s, _) = preceded(multispace0, tag("="))(s)?;
        let (s, ev) = preceded(multispace0, parse_term)(s)?;
        let (s, _) = preceded(multispace0, tag("in"))(s)?;
        let (s, e) = preceded(multispace0, parse_term)(s)?;

        Ok((s, Term::Let { x, ev: Box::new(ev), e: Box::new(e) }))
    }



    pub fn parse_fix(s: &str) -> IResult<&str, Term> {
        let (s, _) = tag("fun")(s)?;
        let (s, f) = preceded(multispace0, parse_termvar)(s)?;
        let (s, x) = preceded(multispace0, parse_termvar)(s)?;
        let (s, _) = preceded(multispace0, tag("="))(s)?;
        let (s, e) = preceded(multispace0, parse_term)(s)?;

        Ok((s, Term::Fix { f, x, e: Box::new(e) }))
    }



    pub fn parse_parens(s: &str) -> IResult<&str, Term> {
        let (s, _) = tag("(")(s)?;
        let (s, term) = preceded(multispace0, parse_term)(s)?;
        let (s, _) = preceded(multispace0, tag(")"))(s)?;

        Ok((s, term))
    }



    pub fn parse_oneterm(s: &str) -> IResult<&str, Term> {
        alt((
            parse_const,
            parse_abs,
            parse_let,
            parse_fix,
            parse_parens,
            parse_var_as_term
        ))(s)
    }



    pub fn parse_term(s: &str) -> IResult<&str, Term> {
        let (s, term0) = parse_oneterm(s)?;
        let (s, terms) = many0(preceded(multispace0, parse_oneterm))(s)?;

        let term = terms.into_iter()
            .fold(
                term0,
                |term1, term2| {
                    Term::App {
                        e1: Box::new(term1),
                        e2: Box::new(term2),
                    }
                }
            );

        Ok((s, term))
    }



    pub fn parse_var_as_type(s: &str) -> IResult<&str, Type> {
        map(
            parse_typevar,
            |tv| Type::Var { var: tv }
        )(s)
    }



    pub fn parse_func(s: &str) -> IResult<&str, Type> {
        let (s, f) = parse_id(s)?;
        let (s, _) = preceded(multispace0, tag("("))(s)?;
        let (s, tys) = separated_list0(
            preceded(multispace0, tag(",")),
            preceded(multispace0, parse_type)
        )(s)?;
        let (s, _) = preceded(multispace0, tag(")"))(s)?;

        Ok((s, Type::Func { f, tys }))
    }



    pub fn parse_type(s: &str) -> IResult<&str, Type> {
        alt((
            parse_func,
            parse_var_as_type
        ))(s)
    }
}



use crate::syntax::{ Term, Type };



use nom::{
    IResult,
    bytes::complete::{ tag },
    character::complete::{ multispace0 },
    combinator::{ eof, recognize },
    sequence::{ tuple },
};



#[derive(Debug, Clone)]
pub enum Parse<T> {
    Success(T),
    Maybe,
    Fail,
}



pub fn parse_term(s: &str) -> Parse<Term> {
    //  Consume as much whitespace as possible.
    let s =
        match multispace0::<&str, ()>(s) {
            Ok((s, _)) => s,
            _ => {
                eprintln!("Strange error: whitespace0 failed.");
                return Parse::Fail;
            },
        };

    //  Parse a term.
    let (s, parse) = match self::internal::parse_term(s) {
        Ok((s, term)) => (s, Parse::Success(term)),
        Err(nom::Err::Incomplete(_)) => {
            return Parse::Maybe;
        },
        _ => {
            eprintln!("Error: failed to parse term.");
            return Parse::Fail;
        },
    };

    //  Check if we are at ; EOF.
    let res: IResult<&str, &str> = recognize(tuple((multispace0, tag(";"), multispace0, eof)))(s);
    match res {
        Ok(_) => parse,
        _ => {
            eprintln!("Error: trailing bytes.");
            Parse::Fail
        },
    }
}



pub fn parse_type(s: &str) -> Parse<Type> {
    //  Consume as much whitespace as possible.
    let s =
        match multispace0::<&str, ()>(s) {
            Ok((s, _)) => s,
            _ => {
                eprintln!("Strange error: whitespace0 failed.");
                return Parse::Fail;
            },
        };

    //  Parse a term.
    let (s, parse) = match self::internal::parse_type(s) {
        Ok((s, ty)) => (s, Parse::Success(ty)),
        Err(nom::Err::Incomplete(_)) => {
            return Parse::Maybe;
        },
        _ => {
            eprintln!("Error: failed to parse type.");
            return Parse::Fail;
        },
    };

    //  Check if we are at ; EOF.
    let res: IResult<&str, &str> = recognize(tuple((multispace0, tag(";"), multispace0, eof)))(s);
    match res {
        Ok(_) => parse,
        _ => {
            eprintln!("Error: trailing bytes.");
            Parse::Fail
        },
    }
}
