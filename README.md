# `types`

An implementation of [Algorithm M](https://ropas.snu.ac.kr/~kwang/paper/98-toplas-leyi.pdf).

The unification algorithm implemented here is described in [Frank Pfenning's lecture notes](https://www.cs.cmu.edu/~fp/courses/lp/lectures/06-unif.pdf).
